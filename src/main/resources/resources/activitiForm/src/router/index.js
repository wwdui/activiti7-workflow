import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)
import Layout from '@/layout'
export const constantRoutes = [
  {
    path: '/',
    name: 'complex',
    redirect: '/login',
    component: Layout,
    label: '流程管理',
    meta: { title: '流程管理', icon: '&#xe6cd;', roles: ['province', 'admin'] },
    zoom: 2,
    id: 1,
    children: [
      {
        id: 11,
        path: 'complex',
        name: 'complex',
        meta: { title: '首页', icon: '&#xe642;' },
        component: () => import('@/views/index/guide')
      },
      {
        id: 12,
        path: 'deploy',
        name: 'deploy',
        meta: { title: '流程部署列表', icon: '&#xe8cf;' },
        component: () => import('@/views/deploy')
      },
      {
        id: 13,
        path: 'example',
        name: 'example',
        meta: { title: '流程实例列表', icon: '&#xe8cf;' },
        component: () => import('@/views/example')
      },
      {
        id: 14,
        path: 'backlog',
        name: 'backlog',
        meta: { title: '待办任务', icon: '&#xe624;' },
        component: () => import('@/views/backlog'),
        hidden: true
      },
      {
        id: 15,
        path: 'history',
        name: 'history',
        meta: { title: '历史任务', icon: '&#xe624;' },
        component: () => import('@/views/history'),
        hidden: true
      },
      {
        id: 16,
        path: 'formTab',
        name: 'formTab',
        meta: { title: '动态表单', icon: '&#xe624;' },
        component: () => import('@/views/formTab'),
        hidden: true
      },
      {
        id: 17,
        path: 'leave',
        name: 'leave',
        meta: { title: '我的请假', icon: '&#xe624;' },
        component: () => import('@/views/leave')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/Login/index'),
    hidden: true,
    label: 'hidden'
  }
]

const createRouter = () =>
  new Router({
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
  })

const router = createRouter()

export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher
}

export default router
