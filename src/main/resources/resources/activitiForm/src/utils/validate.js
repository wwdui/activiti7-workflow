/**
 * Created by PanJiaChen on 16/11/18.
 */

/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}

/**
 * 邮箱验证
 * @param {*} value
 * @returns
 */
const checkEmail = (rule, value, callback) => {
  const regEmail = /^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/
  if (regEmail.test(value)) {
    return callback()
  }
  callback(new Error('邮箱格式不正确；例：xxx@.com'))
}

/**
 * 手机验证
 * @param {*} value
 * @returns
 */
const checkMobile = (rule, value, callback) => {
  const regMobile = /^1(3[0-9]|4[5,7]|5[0,1,2,3,5,6,7,8,9]|6[2,5,6,7]|7[0,1,7,8]|8[0-9]|9[1,8,9])\d{8}$/
  if (!value) {
    return callback()
  } else {
    if (regMobile.test(value)) {
      return callback()
    }
    callback(new Error('请输入11位手机号； 例：131*****098'))
  }
}

/**
 * 座机号码、传真号码
 * @param {*} value
 * @returns
 */
const checkTelOrFox = (rule, value, callback) => {
  const regTelOrFox = /^(\d{3,4}-)?\d{7,8}$/
  if (!value) {
    return callback()
  } else {
    if (value.length < 10 || value > 13 || !regTelOrFox.test(value)) {
      callback(new Error('请输入正确的传真号； 例：029-12****7'))
    } else {
      return callback()
    }
  }
}

/**
 * 车牌号验证
 * @param {*} value
 * @returns
 */
const checkCarCode = (rule, value, callback) => {
  const regCarCode = /^(([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领][A-Z](([0-9]{5}[DF])|([DF]([A-HJ-NP-Z0-9])[0-9]{4})))|([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领][A-Z][A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳使领]))$/
  if (regCarCode.test(value)) {
    return callback()
  }
  callback(new Error('请输入正如的车牌号；例：冀H12**1'))
}

/**
 * 车牌号验证
 * @param {*} value
 * @returns
 */
const checkCarNumber = (rule, value, callback) => {
  const regCarCode = /^[1-8]\d{11}$/
  if (regCarCode.test(value)) {
    return callback()
  }
  callback(new Error('请输入12位驾驶证号；例：4340******23'))
}

/**
 * 输入的时间大于当前时间
 * @param {*} value
 * @param {*} callback
 * @returns
/**
 *
 *
 * @param {*} rule
 * @param {*} value
 * @param {*} callback
 * @return {*}
 */
const moreThanNow = (rule, value, callback) => {
  var myDate = new Date()
  var now = myDate.valueOf()
  let time
  if (value instanceof Array) {
    time = new Date(`${value[1]} 23:59:59`).valueOf()
  } else {
    time = new Date(value).valueOf()
  }
  if (time > now || time === now) {
    return callback()
  } else {
    callback(new Error('结束日期不能小于当前日期'))
  }
}

/**
 * 输入的时间小于当前时间
 * @param {*} value
 * @param {*} callback
 * @returns
 */
const lessThanNow = (rule, value, callback) => {
  var myDate = new Date()
  var now = myDate.valueOf()
  let time
  if (value instanceof Array) {
    time = new Date(value[1]).valueOf()
  } else {
    time = new Date(value).valueOf()
  }

  if (now >= time) {
    return callback()
  } else {
    callback(new Error('日期不能大于当前时间'))
  }
}

/**
 * 邮政编码
 * @param {*} value
 * @returns
 */
const checkPostCode = (rule, value, callback) => {
  const regPostCode = /^[0-9]{6}$/
  if (!value) {
    return callback()
  } else {
    if (regPostCode.test(value)) {
      return callback()
    }
    callback(new Error('邮政编码格式不正确'))
  }
}

/**
 * 纯数字
 * @param {*} value
 * @returns
 */
const onlyNumber = (rule, value, callback) => {
  const regNumber = /^((-?[0-9])|0|(-?[0-9]\d*\.?\d+)|(0\.\d+))$/
  if (regNumber.test(value)) {
    return callback()
  }
  callback(new Error('请输入数值'))
}

/**
 * 校验经度
 * @param {*} value
 * @returns
 */
const checkLon = (rule, value, callback) => {
  const regNumber = /^(\-|\+)?(((\d|[1-9]\d|1[0-7]\d|0{1,3})\.\d{0,6})|(\d|[1-9]\d|1[0-7]\d|0{1,3})|180\.0{0,6}|180)$/
  if (regNumber.test(value)) {
    return callback()
  }
  callback(new Error('请输入正确经度；例：103.9238'))
}

/**
 * 校验纬度
 * @param {*} value
 * @returns
 */
const checkLat = (rule, value, callback) => {
  const regNumber = /^(\-|\+)?([0-8]?\d{1}\.\d{0,6}|90\.0{0,6}|[0-8]?\d{1}|90)$/
  if (regNumber.test(value)) {
    return callback()
  }
  callback(new Error('请输入正确纬度；例：33.0238'))
}

export { checkEmail, checkMobile, checkTelOrFox, checkPostCode, moreThanNow, lessThanNow, onlyNumber, checkCarCode, checkCarNumber, checkLon, checkLat }
