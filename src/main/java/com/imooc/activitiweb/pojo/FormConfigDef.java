package com.imooc.activitiweb.pojo;

public class FormConfigDef {
    private String taskid;

    private String formconfig;

    private String widgetlist;

    public String getTaskid() {
        return taskid;
    }

    public void setTaskid(String taskid) {
        this.taskid = taskid == null ? null : taskid.trim();
    }

    public String getFormconfig() {
        return formconfig;
    }

    public void setFormconfig(String formconfig) {
        this.formconfig = formconfig == null ? null : formconfig.trim();
    }

    public String getWidgetlist() {
        return widgetlist;
    }

    public void setWidgetlist(String widgetlist) {
        this.widgetlist = widgetlist == null ? null : widgetlist.trim();
    }

    @Override
    public String toString() {
        return "FormConfigDef{" +
                "taskid='" + taskid + '\'' +
                ", formconfig='" + formconfig + '\'' +
                ", widgetlist='" + widgetlist + '\'' +
                '}';
    }
}